-- find all artist that has letter d in its name.
SELECT * FROM artists WHERE name LIKE "%d%";

--find all songs that has a length of less than 230
SELECT * FROM songs WHERE length < 230;

--join the albums and songs table (Only show the album name, song name and song length)
SELECT albums.album_title, songs.song_name, songs.length FROM albums JOIN songs ON albums.id = songs.album_id;

-- join the artist and albums tables (find all albums that has letter a in its name)
SELECT * FROM albums
JOIN artists ON  albums.artist_id = artists.id WHERE albums.album_title LIKE "%a%";


-- sort the albums in Z-A order (show only the first 4 records)
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- join the albums and songs tables (sort albums from Z-A)
SELECT * FROM albums
JOIN songs ON albums.id = songs.album_id ORDER BY albums.album_title DESC;